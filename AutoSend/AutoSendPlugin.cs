﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlueBird_Plugin;

namespace AutoSend
{
    public class AutoSendPlugin : ISchedulePlugin
    {
        public string name { get { return "自动发推插件"; } }

        public string info { get { return "根据设定，让角色定时发送推特"; } }

        public string key { get { return "Mengluu-AutoSend-Schedule-Plugin"; } }

        public ISchedule[] GetSchedule(int count)
        {
            List<AutoSendSchedule> autoSendSchedules = new List<AutoSendSchedule>();
            AutoSendWindow autoSendWindow = new AutoSendWindow();
            if (autoSendWindow.ShowDialog() == true)
            {
                for (int i = 0; i < count; i++)
                {
                    AutoSendSchedule playSchedule = new AutoSendSchedule(autoSendWindow.time, autoSendWindow.tags);
                    autoSendSchedules.Add(playSchedule);
                }
            }
            return autoSendSchedules.ToArray();
        }
    }
}
