﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AutoSend
{
    static class Bullet
    {
        static string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        static Random random = new Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000);
        public static string GetBullet()
        {
            if (File.Exists(Path.Combine(path, "AutoSendTweet.txt")))
            {
                string[] bullets = File.ReadAllLines(Path.Combine(path, "AutoSendTweet.txt"));
                if (bullets.Length > 0)
                {
                    return bullets[random.Next(0, bullets.Length)];
                }
            }
            return "自动发送推文，当前推文库为空，请在AutoSendTweet.txt中添加内容";
        }
    }
}
