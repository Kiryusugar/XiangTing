﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutoSend
{
    /// <summary>
    /// AutoSendWindow.xaml 的交互逻辑
    /// </summary>
    public partial class AutoSendWindow : Window
    {
        public int time = 600;
        public List<string> tags = new List<string>();
        public AutoSendWindow()
        {
            InitializeComponent();
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            int tmp;
            time = (int.TryParse(IntervalTime.Text, out tmp)) ? tmp : time;
            string[] taglist = TagList.Text.Split('|');
            tags = taglist.ToList();
            DialogResult = true;
            Close();
        }
    }
}
