﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlueBird_Plugin;

namespace AutoSend
{
    public class AutoSendSchedule : ISchedule
    {
        bool isCompleted = false;
        int last_time = 0;
        int interval_time = 600;
        List<string> tags;
        int count = 0;
        public bool completed { get { return isCompleted; } }

        public string name { get { return "自动发送推文"; } }
        public AutoSendSchedule(int time,List<string> taglist)
        {
            interval_time = time;
            tags = taglist;
        }
        int GetTime()
        {
            return DateTime.Now.Second + DateTime.Now.Minute * 60 + DateTime.Now.Hour * 3600 + DateTime.Now.Day * 3600 * 24;
        }
        public void End()
        {
            isCompleted = true;
        }

        public List<ITask> GetTask(ICharacter character)
        {
            int now = GetTime();
            if (now - last_time < interval_time)
            {
                return new List<ITask>();
            }
            count++;
            last_time = now;
            return new List<ITask>() { new SimpletweetTask(new ITask.HALDLE_INFO { type = ITask.HALDLE_TYPE.TARGET, id = character.character_id }, Bullet.GetBullet(), tags, 1) };
        }

        public string ToInfo()
        {
            string info = "自动发送推文\n";
            info += "已发布" + count.ToString() + "条推文\n";
            return info;
        }
    }
}
